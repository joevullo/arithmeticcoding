/*
 * Model.h
 *
 *  Created on: 3 Dec 2014
 *      Author: jvullo
 */

#ifndef MODEL_H_
#define MODEL_H_

class Model {
public:
	Model();
	virtual ~Model();
};

#endif /* MODEL_H_ */
