/*============================================================================
 Name        : coding.cpp
 Author      :
 Version     :
 Copyright   : Your copyright notice
 Description :

 Design, implement and test a program to calculate the arithmetic coding for a given text
 string. The coding can be verified by implementing the decoding to verify that the original
 string can be regenerated. A double precision floating point data type can be used to store
 the coded value but be aware that under the IEEE 745 standard the 52 bit mantissa will
 provide only up to 16 digits of accuracy. Therefore the length of string to be encoded should
 be suitably restricted.

============================================================================

//Pseudocode taken from the ccox lecture.

//Set low to 0.0.
//Set high to 1.0.

//While there are still input symbols do.
	//get an input symbol.
	//range = high-low.
	//low = low + low_range(symbol) * range.
	//high = low + high_range(symbol) * range.
//end while
//output low.
 *
============================================================================
 */

#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <string>

using namespace std;

double encode_symbol(string);
string decode_symbol(double);

/**
 * Get the low range for this char for the char passed as a parameter.
 */
double get_low_range(char c)
{

}

/**
 * Get the high range for the char passed as a parameter.
 */
double get_high_range(char c)
{

}

/**
 * Produces a double representing an encoding symbol/string passed in.
 */
double encode_symbol(string symbol)
{

	double low = 0.0;
	double high = 1.0;

	int i = 0;
	char c;

	// While there are still input symbols to.
	while (symbol[i] != NULL) {
		// Get an input symbol.
		c = symbol[i];
		double range = high - low;
		high = low + range * get_high_range(c);
		low = low + range * get_low_range(c);
		i++;
	}

	//return low;
	//This turns a value in the middle of that range.
	return low + (high-low)/2;
}


/**
 * Decodes the symbol and produces a string representing that value.
 */
string decode_symbol(double value)
{

	string result;
	double high = 1.0;
	double low = 0.0;

	//TODO - Finish this off, needs to get the symbol.
	for ( ; ; )
	{
		double range = high - low;
		//char c = model.getSymbol((message - low)/range);
		char c;
		result += c;
		if ( c == 'Z' )
			return result;
		high = low + range * get_high_range(c);
		low = low + range * get_low_range(c);
	}
}

/**
 * Calculates the probablities of the string and stores them
 * for later use.
 */
void store_probablities()
{

}

/**
 * Asks for a input word and then displays the encoded word and then decodes the word
 * and displays that to the user.
 */
void user_mode()
{

	cout <<  "Arithmetic Coding Program .\n";

	string enteredString;
	double encoded_symbol_value = 0;

	//Asking for symbol to encode.
	cout << "Please enter a symbol to encode: \n";
	cin >> enteredString;
	cout << "Word to encode is: " << enteredString << endl;

	//Encode the word.
	encoded_symbol_value = encode_symbol(enteredString);

	//Print the encoded value now.
	std::cout << "Encoded symbol value: "
			<< encoded_symbol_value
			<< std::setprecision(15)
	<< endl;

	//Decode the value
	string result = decode_symbol(encoded_symbol_value);
	//Print the decoded value for comparison.
	std::cout << "Decompressed result: " << result << "\n";

}


/**
 * The main, currently runs the user mode.
 */
int main()
{
	user_mode();
	return EXIT_SUCCESS;

}










