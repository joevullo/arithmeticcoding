#include <iostream>
#include <iomanip>
#include <string>

//The Structures for the probability.

struct {
  static std::pair<double,double> getProbability( char c )
  {
	  //If the value is upper case.
    if (c >= 'A' && c <= 'Z')
      return std::make_pair( (c - 'A') * .01, /**/ (c - 'A') * .01 + .01);
    //If its lower case.
    else if (c >= 'a' && c <= 'z')
      return std::make_pair( (c - 'a') * .02 + 0.30, /**/ (c - 'a') * .02 + .02 + 0.30);
    else
      throw "character out of range";
  }

  static char getSymbol(double d)
  {
    if ( d >= 0.0 && d < 0.26)
      return 'A' + static_cast<int>(d*100);
    else if ( d >= 0.3 && d < 0.82)
      return 'a' + static_cast<int>((d-0.3)*50);
    else
      throw "message out of range";
  }
} model;

//Encoding and Decoding.
//
//double compress( std::string s)
//{
//  double high = 1.0;
//  double low = 0.0;
//  for ( char c : s ) {
//    std::pair<double,double> p = model.getProbability(c);
//    double range = high - low;
//    high = low + range * p.second;
//    low = low + range * p.first;
//  }
//
//  return low + (high-low)/2;
//}

std::string decompress(double message)
{
  std::string result;
  double high = 1.0;
  double low = 0.0;
  for ( ; ; )
  {
    double range = high - low;
    char c = model.getSymbol((message - low)/range);
    result += c;
    if ( c == 'Z' )
      return result;
    std::pair<double,double> p = model.getProbability(c);
    high = low + range * p.second;
    low = low + range * p.first;
  }
}

//Main for the file.

//int main(int, char **)
//{
//  double val = compress("WXYZ");
//  //double val = compress("abcdWXYZ"); //this will be more interesting
//  std::cout << "Compressed value: "
//            << std::setprecision(15)
//            << val << "\n";
//  std::string result = decompress(val);
//  std::cout << "Decompressed result: " << result << "\n";
//  return 0;
//}
