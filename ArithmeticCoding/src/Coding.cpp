/*============================================================================
 Name        : coding.cpp
 Author      :
 Version     :
 Copyright   : Your copyright notice
 Description :

 Design, implement and test a program to calculate the arithmetic coding for a given text
 string. The coding can be verified by implementing the decoding to verify that the original
 string can be regenerated. A double precision floating point data type can be used to store
 the coded value but be aware that under the IEEE 745 standard the 52 bit mantissa will
 provide only up to 16 digits of accuracy. Therefore the length of string to be encoded should
 be suitably restricted.

============================================================================
 */

#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <string>

#define MAX_INPUT_LENGTH 20

using namespace std;

const string ASCII_RESTRICTION = "Current only upper case letters, no punctuation, number or any special "
		"characters are allowed. Please also enter a full stop at the end of the word. ";

 const char DELIMITER = '.';
 const string ALPHA_UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
// //Probabilities are out of a 100%.
// const double ALPHA_PROBABILITIES [] = {8.12, 1.49 , 2.71 , 4.32 , 12.02 , 2.3 , 2.03 , 5.92,
//		7.31 , 0.1 , 0.69 , 3.98 , 2.61 ,6.95 , 7.68 ,1.82 , 0.11 , 6.02 , 6.28 , 9.1,
//		2.88 , 1.11 , 2.09 , 0.17, 2.11 , 0.07};

//Probabilities of letters are from http://www.math.cornell.edu/~mec/2003-2004/cryptography/subs/frequencies.html

// Added in a fullstop in the range of letters to use as a delimiter. I took some of the probability from E
 const string DATA_RANGE = ALPHA_UPPER + DELIMITER;
 const double DATA_PROBABILITES [] = {8.12, 1.49 , 2.71 , 4.32 , 11.02 , 2.3 , 2.03 , 5.92,
		7.31 , 0.1 , 0.69 , 3.98 , 2.61 ,6.95 , 7.68 ,1.82 , 0.11 , 6.02 , 6.28 , 9.1,
		2.88 , 1.11 , 2.09 , 0.17, 2.11 , 0.07, 1};


//Limited version of total character set for testing.
//const string ALPHA_UPPER = "GIVENSTRIN";
//const double ALPHA_PROBABILITIES [] = {10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10, 10};


/**
 * Get the low range for this char for the char passed as a parameter.
 */
double get_low_range(char c)
{
	double low_range = 0;
	//For loop through the letters.
	for (unsigned int i = 0; i < DATA_RANGE.size(); i++) {
		//When the letter matches, return the current value of range.
		if (DATA_RANGE.at(i) == c) {
			return low_range / 100; // Divide by a 100 because the probabilities are currently out of 100%
		}
		//If you don't return, add the current probability to the range and loop range.
		low_range += DATA_PROBABILITES[i];
	}
	// Return 0, if the for loop fails.
	return 0;
}

/**
 * Get the high range for the char passed as a parameter.
 */
double get_high_range(char c)
{
	double high_range = 0;
	//For loop through the letters.
	for (unsigned int i = 0; i < DATA_RANGE.size(); i++) {
			//Add probability to the range before returning, this will produce the high range.
			high_range += DATA_PROBABILITES[i];
			//When the letter matches, return the current value of range.
			if (DATA_RANGE.at(i) == c) {
				return high_range / 100; // Divide by a 100 because the probabilities are currently out of 100%
			}
	}
	// Return 0, if the for loop fails.
	return 0;
}

/**
 * Get the character by entering the double value, also use the parameters to return the high and low ranges
 * of that value.
 */
char get_symbol(double value, double &high, double &low) {

	double range = 0;

	//For the length of the probabilities.
	for (unsigned int i = 0; i < sizeof(DATA_PROBABILITES); i++) {
		range += DATA_PROBABILITES[i] / 100;
		double low_range = range - DATA_PROBABILITES[i] / 100; //Probilities are out of 100, need to be between 0 & 1.
		double high_range = range;
		// 0.9 <= X < 1.0 - Week 9 slides.
		//more than or equal to the low range, and less than the high range.
		if (low_range <= value && value < high_range) {
			//Set the parameters to the correct range.
			high = high_range;
			low = low_range;
			// Return the letter for the position that is valid.
			return DATA_RANGE[i];
		}
	}
	// Return the delimiter if you can't find anything.
	return DELIMITER;
}

/**
 * Produces a double representing an encoding symbol/string passed in.
 */
double encode_symbol(string symbol)
{
	double low = 0.0;
	double high = 1.0;

	int i = 0;
	char c;

	// While there are still input symbols to.
	while (symbol[i] != NULL) {
		// Get an input symbol.
		c = symbol[i];
		//cout << c << endl;
		double range = high - low;
		high = low + get_high_range(c) * range;
		low = low + get_low_range(c) * range;
		i++;
	}

	//return low;
	//This turns a value in the middle of that range.
	return low + (high-low)/2;
}

/**
 * Decodes the symbol and produces a string representing that value.
 *
 * Because the model contains the full alphabet, we won't hit 0 because
 * we only have 16 digits of accuracy. If we limited the range then we could
 * keep going until all the letters have been removed.
 *
 * As a solution I have added a "delimiter" which is currently a fullstop which should be added
 * to the end of the input string
 */
string decode_symbol(double value)
{

	string result = "";
	double high = 1.0;
	double low = 0.0;

	// Keep going for the max input length.
	for (int i = 0; i < MAX_INPUT_LENGTH; i++)
	{
		char c = get_symbol(value, high, low);
		//cout << "C = " << c << " V = "  << value << endl;
		result += c;

		// Stop if you get to the delimiter.
		if (c == DELIMITER)
			return result;

		double range = high - low; //Range is the difference between high and low.
		value -= low; //Remove the low from the value.
		value /= range; //Divide the value by the range.
	}

	//Return the result that you have, though it it likely to long because
	//Hasn't found the delimiter in this case.
	return result;
}

/**
 * Runs encode and decode with some print outs to display to the user.
 */
void run_encode_and_decode(string input)
{
	double encoded_symbol_value = 0;

	cout << "Word to encode is: " << input << endl;

	encoded_symbol_value = encode_symbol(input);

	//Print the encoded value now.
	std::cout << "Encoded symbol value: "
			<< encoded_symbol_value
			<< std::setprecision(15) //Ensure that
	<< endl;

	//Decode the value
	string result = decode_symbol(encoded_symbol_value);
	//Print the decoded value for comparison.
	cout << "De-encoded result: " << result << "\n";
}

/**
 * Asks the user to input a string, also letting them know the current input restrictions.
 *
 * Returns the entered string.
 */
string ask_user_for_input()
{
	string enteredString;

	//Asking for symbol to encode.
	cout << "Please enter a symbol to encode: " << endl;
	cout << "" << ASCII_RESTRICTION << endl; //Prints the current restricted ASCII code set that you can input.
	cin >> enteredString;

	//If the input is too long.
	if (enteredString.size() >= MAX_INPUT_LENGTH) {
		cout << "Incorrect, please try again. " << endl;
		enteredString = ask_user_for_input();
	}

	return enteredString;
}


/**
 * Tests get symbol.
 */
void test_get_symbol(double values[], int length) {

	cout << endl << "Test Get Symbol" << endl;

	double high = 0, low = 0; //These are  never actually used, but required.

	for (int i = 0; i < length; i++) {
			cout << values[i] << ": " << get_symbol(values[i],high,low) << endl;
	}
}

/**
 * Uses the array parameter to test that the output of decode is correct.
 */
void test_decode(double values[], int length) {

	cout << endl << "Test Decode" << endl;

	for (int i = 0; i < length; i++) {
			cout << values[i] << " = " << decode_symbol(values[i]) << endl << std::setprecision(15);//Ensure that;
	}
}

/**
 * Uses the array parameter to check that encoding is correct.
 */
bool test_encode(string *stringArray, int length) {

	double values[length];

	cout << endl << "Test Encode" << endl;

	for (int i = 0; i < length; i++) {
		values[i] = encode_symbol(stringArray[i]);
		//Outputting the results for human comparison.
		cout << stringArray[i] << " = " << encode_symbol(stringArray[i]) << endl;
 	}

	cout << endl << "Recheck" << endl;

	//Compares the decoded results, returns false if they don't match.
	for (int i = 0; i < length; i++) {
		string v = decode_symbol(values[i]);
		cout << stringArray[i] << " = " << decode_symbol(values[i]) << endl;
		if(v.compare(stringArray[i])) {
			return false;
		}
	}

	// Test passed.
	return true;

}

/**
 * Uses the array parameter to test the correct low range is found.
 *
 * Best way to test is to manually check the output.
 */
void test_low_range(char *charArray, int length) {

	cout << endl << "Test Low Range" << endl;

	for (int i = 0; i < length; i++) {
		cout << charArray[i] << ": " << get_low_range(charArray[i]) << endl;
 	}
}

/**
 * Uses the array parameter to test the high range.
 *
 * Best way to test is to manually check the output.
 */
void test_high_range(char *charArray, int length) {

	cout << endl << "Test High Range" << endl;

	for (int i = 0; i < length; i++) {
		cout << charArray[i] << ": " << get_high_range(charArray[i]) << endl;
 	}
}

/**
 * Prints out the ranges that have been given for.
 */
void test_all_ranges()
{
	cout << endl << "Test All Ranges" << endl;

	cout << "Data Range: " << DATA_RANGE << endl << endl;

	cout << "Data Probabilities" << endl;
	for (unsigned int i = 0; i < DATA_RANGE.size(); i++) {
		cout << DATA_RANGE.at(i) << " : " <<  DATA_PROBABILITES[i] << endl;
	}

	cout << endl;

	//For the alphabet, tell me the letter, and its high and low range.
	for (unsigned int i = 0; i < DATA_RANGE.size(); i++) {
		cout << DATA_RANGE.at(i)
				<< " L: " << get_low_range(DATA_RANGE.at(i))
				<< " H: " << get_high_range(DATA_RANGE.at(i)) << endl;
	}
}

/**
 * Wrapper that runs all the tests.
 */
void test_all()
{
	int length = 5;
	double testDecode [] = {0.332878906727981,0.525715311379,0.528861794050418,0.438623847532,0.452816699716864};
	string testWords [] = {"HELLO.","MY.","NAME.","IS.","JOE."};
	char testChars [] = {'A','E','F','H','J'};

	test_all_ranges();
	test_decode(testDecode, length); //Expect testEncode array.
	test_encode(testWords, length); //Expect testDecode array.
	test_get_symbol(testDecode, length); //Expected H M N I J.
	test_high_range(testChars, length); //Expected: 0.0812 , 0.2766, 0.2996 , 0.3791, 0.4532.
	test_low_range(testChars, length); //Expected: 0 , 0.1664, 0.2766 , 0.3199, 0.4522.
}


/**
 * The main, currently runs the user mode.
 */
int main()
{
	cout <<  "Arithmetic Coding Program." <<endl;

	test_all();

	//While so you can keep entering input.
	while (true) {
		cout << endl;
		string input = ask_user_for_input(); //This asks for the users input & lets the user know
		run_encode_and_decode(input); //This runs encode and decode on that string and produces the results.
	}

	return EXIT_SUCCESS;
}










